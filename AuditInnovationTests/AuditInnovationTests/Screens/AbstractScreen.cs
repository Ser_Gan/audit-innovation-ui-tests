﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AuditInnovationTests
{
	public class AbstractScreen 
	{
		public IApp app = BaseTest.app;
		private string searchingElementName;


		//Verify if element displays on screen
		public bool IsElementAppeared (int seconds, Func<AppQuery, AppQuery> element) {
			try {
				app.WaitForElement (element, "timeOut for waiting element",TimeSpan.FromSeconds(seconds), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(0));
			} catch (Exception e)
			{
				searchingElementName = e.ToString ();
				Utils.TakeScreenShot (searchingElementName+".png");
				return false;
			}
			return true;
		}


		//Return element which is 100% displayed on the screen
		public Func<AppQuery, AppQuery> AppElement (Func<AppQuery, AppQuery> element) {
			if (IsElementAppeared (4, element)) {
				return element;
			}
			else 
			{
				throw new TimeoutException ("Element " + searchingElementName + " not found on screen");
				return null;
				}
		}

		public void Tap (Func<AppQuery, AppQuery> locator){
			var elementToTap = AppElement (locator);
			app.Tap (elementToTap);
		}

		public void DoubleTap (Func<AppQuery, AppQuery> locator) {
			var elementToTap = AppElement (locator);
			app.DoubleTap(elementToTap);
		}

		public void EnterText (Func<AppQuery, AppQuery> locator, string text) {

			var elementToEnter = AppElement (locator);
			app.ClearText (elementToEnter);
			app.EnterText (elementToEnter, text);

			//JUST FOR DEV VERSION!!!!. BUG IN APP. KEYBOARD SHOULD BE HIDDEN. ANDROID ONLY
			//NavigateBack();
		}

		public void ClearField (Func<AppQuery, AppQuery> locator) {

			var elementToClear = AppElement (locator);
			app.ClearText (elementToClear);
		}


		public void SwipeToRight () {

			app.SwipeRight();
		}

		public void SwipeToLeft () {

			app.SwipeLeft ();
		}

		public void ScrollDown () {
			app.ScrollDown ();
		}

		public void ScrollUp () {
			app.ScrollUp ();
		}
			

		public bool isMessageAppeared (string messageText) {
			Func <AppQuery, AppQuery> errorMessageElement = c => c.Marked (messageText);

			return IsElementAppeared (2,errorMessageElement);
		}
			

		public void TapYES () {
			Tap (c => c.Marked ("YES")); 
		}

		public void TapNO () {
			Tap (c => c.Marked ("NO"));
		} 

		public void NavigateBack (){
			app.Back ();
		}

		public void PressEnter () {
			app.PressEnter ();
		}

		public void WaitForElement (Func<AppQuery, AppQuery> element, int seconds) {
			app.WaitForElement (element, "timeOut for waiting element",TimeSpan.FromSeconds(seconds), TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(0));
		}

		public void SwipeToRightLong()
		{
			AppResult rootView = app.Query (x => x.All ()) [0];

			//Will try to scroll +/-100 from the vertical center point
			float gap = 150;
			app.DragCoordinates(rootView.Rect.X, rootView.Rect.CenterY, rootView.Rect.CenterX + gap, rootView.Rect.CenterY);

		}
			
			

		public void PullToRefresh (){
			AppResult rootView = app.Query (x => x.All ()) [0];

			//Will try to scroll +/-100 from the vertical center point
			float gap = 150;
			app.DragCoordinates(rootView.Rect.CenterX, rootView.Rect.CenterY + gap, rootView.Rect.CenterX, rootView.Rect.CenterY - gap);
								
		}



	}
}


