﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AuditInnovationTests
{
	public class ContactsScreen : NavigationBarScreen
	{
		Platform platform;
		public ContactsScreen (Platform platform)
		{
			this.platform = platform;
			if (!(IsElementAppeared(3, CONTACTS_TITLE))) 

			{
				throw new Exception("Contacts screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> CONTACTS_TITLE = c => c.Marked ("Contacts");
	}
}

