﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AuditInnovationTests
{
	public class LoginScreen : AbstractScreen
	{
		Platform platform;
		public LoginScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();


		}




		public Func <AppQuery, AppQuery> LOGIN_FIELD = c => c.Marked ("login_field");
		private Func <AppQuery, AppQuery> PASSWORD_FIELD = c => c.Marked ("pass");
		private Func <AppQuery, AppQuery> LOGIN_BUTTON = c => c.Button ("Loginn");
		private Func <AppQuery, AppQuery> ERROR_MESSAGE = c => c.Text ("Incorrect login or password.");



		public LoginScreen EnterLogin (String userLogin) {
			EnterText (LOGIN_FIELD, userLogin);
			return this;
		}

		public LoginScreen EnterPassword (String userPassword) {
			EnterText (PASSWORD_FIELD, userPassword);
			return this;
		}

		public void SubmitSignIn () {
			Tap (LOGIN_BUTTON);
		}


		public NewsFeedScreen SignIn (string userLogin, string userPassword){
			EnterLogin (userLogin);
			EnterPassword (userPassword);
			SubmitSignIn ();
			return new NewsFeedScreen (platform);
		}

		public LoginScreen SignInWithWrongCredantials (string mail, string password) {
			EnterLogin (mail);
			EnterPassword (password);
			SubmitSignIn ();
			return this;
		}
			
		public bool isErrorMessageAppeared (){
			return IsElementAppeared (3,ERROR_MESSAGE);
		}







	}
}

