﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AuditInnovationTests
{
	public class NavigationBarScreen : AbstractScreen
	{
		Platform platform;
		public NavigationBarScreen ()
		{
			this.platform = platform;
			//choosePlatform ();
		}

		private Func <AppQuery, AppQuery> NEWS_FEED_BUTTON = c => c.Button ("..........");
		private Func <AppQuery, AppQuery> SURVEYS_BUTTON = c => c.Button ("..........");
		private Func <AppQuery, AppQuery> CONTACTS_BUTTON = c => c.Button ("..........");

		public NewsFeedScreen NavigateToNewsFeed () {
			Tap (NEWS_FEED_BUTTON);
			return new NewsFeedScreen (platform);
		}

		public SurveysScreen NavigateToSurveys () {
			Tap (SURVEYS_BUTTON);
			return new SurveysScreen (platform);
		}

		public ContactsScreen NavigateToCOntacts () {
			Tap (CONTACTS_BUTTON);
			return new ContactsScreen (platform);
		}
	}
}

