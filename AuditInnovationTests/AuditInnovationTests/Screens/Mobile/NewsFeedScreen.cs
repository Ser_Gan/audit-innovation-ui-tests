﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AuditInnovationTests
{
	public class NewsFeedScreen : NavigationBarScreen
	{
		Platform platform;
		public NewsFeedScreen (Platform platform)
		{
			this.platform = platform;
			if (!(IsElementAppeared(3, NEWS_FEED_TITLE))) 

			{
				throw new Exception("News Feed screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> NEWS_FEED_TITLE = c => c.Marked ("News Feed");
	}
}

