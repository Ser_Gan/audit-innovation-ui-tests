﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AuditInnovationTests
{
	public class SurveysScreen : NavigationBarScreen
	{
		Platform platform;
		public SurveysScreen (Platform platform)
		{
			this.platform = platform;
			if (!(IsElementAppeared(3, SURVEYS_FEED_TITLE))) 

			{
				throw new Exception("SURVEYS Feed screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> SURVEYS_FEED_TITLE = c => c.Marked ("Surveys Feed");
	}
}

