﻿using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using System;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace AuditInnovationTests
{
	[TestFixture]
	public class LoginTest : BaseTest
	{
		public LoginTest (Platform platform)
		{
			base.platform = platform;

		}

		//[TestFixtureSetUp]
		public void Setup () {
			//Do some actions, preconditions for all tests in current class
			AppInitializer.StartAppClean (platform);
		}


		[Test]
		public void Testing () {
			Utils.runShellCommand ("adb shell input keyevent 82");

			AppInitializer.StartAppClean (platform);
			LoginScreen loginScreen = new LoginScreen (platform);
			loginScreen.SubmitSignIn ();


			//app.ScrollUpTo (c => c.Marked ("Pittsfield Plaza"),c => c.Class ("UITableViewCellContentView"),ScrollStrategy.Gesture);

			//app.Repl ();
		}


		[Test]
		[Ignore]
		public void LoginToApp () {
			LoginScreen loginScreen = new LoginScreen (platform);
			NewsFeedScreen feedscreen = loginScreen
				.SignIn (userEmail, userPassword);
			app.SwipeLeft ();
			//Assert.False (feedscreen.IsElementAppeared());
			
		}

		[Test]
		[Ignore]
		public void SomeTest () {
		//Any test can be ignored by attribute 
		}


		//[TestFixtureTearDown]
		public void TearDown () {
			// Do some actions.
			//Log Out for example
		}


			
		}


	}


