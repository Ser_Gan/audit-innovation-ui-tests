﻿using System;
using System.IO;
using System.Linq;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AuditInnovationTests
{
	public class AppInitializer
	{
		public static IApp StartAppClean (Platform platform) {
			if (platform == Platform.iOS) 
			{
				return BaseTest.app = ConfigureApp
					.iOS
					//.DeviceIdentifier ("1CEF092A-49A1-4A3E-8B38-B003A7775B7F")
					.InstalledApp ("com.deloitte..............")
					.AppBundle ("../../../iOSAppProject/bin/iPhoneSimulator/Debug/iosapp.app")
					.EnableLocalScreenshots()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.Clear);
			} 
			else 
			{
				return BaseTest.app = ConfigureApp
					.Android
					//.ApkFile (Properties.androidApk)
					.EnableLocalScreenshots()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.Clear);
			}
		}

		public static IApp StartApp (Platform platform) {
			if (platform == Platform.iOS) 
			{
				return BaseTest.app = ConfigureApp
					.iOS
					.DeviceIdentifier ("B788CF90-6A57-4320-A9DB-29A7462993C8")
					.InstalledApp ("com.deloitte............")
					//.AppBundle (Properties.iOSBundle)
					.EnableLocalScreenshots()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.DoNotClear);
			} 
			else 
			{
				return BaseTest.app = ConfigureApp
					.Android
				//.ApkFile (Properties.androidApk) 
					.EnableLocalScreenshots ()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.DoNotClear);
			}
		}

		public static IApp ConnectToApp (Platform platform) {
			if (platform == Platform.iOS) 
			{
				return BaseTest.app = ConfigureApp
					.iOS
					.AppBundle ("...")
					.ConnectToApp ();

			} 
			else 
			{
				return BaseTest.app = ConfigureApp
					.Android
					.ApkFile ("...") 
					.EnableLocalScreenshots ()
					.ConnectToApp ();
			}
		}







	}


}

