﻿using System;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace AuditInnovationTests
{
	public class DriverInitializer
	{

		public void setupDriverFireFox (){
			if (null == BaseTest.WebDriver){
				
				//SetUp Firefox driver
				FirefoxProfile profile = new FirefoxProfile();
				//profile.EnableNativeEvents;
				//profile.SetPreference ("browser.download.dir", DOWNLOAD_LOCATION);

				//Create FF driver
				BaseTest.WebDriver = new FirefoxDriver(profile);
				BaseTest.WebDriver.Manage ().Window.Maximize ();

			
		}
	}
}
}
