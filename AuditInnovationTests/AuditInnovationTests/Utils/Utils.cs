﻿using System;
using Xamarin.UITest;
using System.IO;

namespace AuditInnovationTests
{
	public class Utils
	{
		public Utils ()
		{
			
		}



		public static void TakeScreenShot (string screenshotName) {

			DirectoryInfo dir = Directory.CreateDirectory ("../../../Screenshots/"+BaseTest.tempDirName);
			string path = dir.FullName.ToString ();
			FileInfo screenshot = BaseTest.app.Screenshot ("some title");
			screenshot.CopyTo (path+"/"+screenshotName);
		}
			
		public static bool runShellCommand (string command)  {
			try {
				java.lang.Process p = java.lang.Runtime.getRuntime ().exec(command);
				return processCommandOutput(p);
			}
			catch (Exception e) {
				string s = e.StackTrace.ToString ();
				Console.WriteLine (s);
				throw new EntryPointNotFoundException ();
				return false;
			}
		}


		private static bool processCommandOutput(java.lang.Process p)  {
			String line;
			bool b = true;

			java.io.BufferedReader input =
				new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				Console.WriteLine (line);
			}
			java.io.BufferedReader stdError = new java.io.BufferedReader(new
				java.io.InputStreamReader(p.getErrorStream()));
			while ((line = stdError.readLine()) != null) {
				Console.WriteLine ("Error: "+line);
				b = false;
			}
			return b;
		}
	}
}

